# 99x technical assessment

Sample shopping cart front end application built using vue 2.6

## Assumptions
 - 10 % discount for the carton price and 30 % service charge addition for the single units will not show in the product/price list page (home page) 

## Features

 - UI framework - [vuetify](https://vuetifyjs.com/en/introduction/why-vuetify/#getting-started)
 - State management - [vuex](https://vuex.vuejs.org/)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
