import Vue from 'vue'
import Vuex from 'vuex'
import price from './modules/price'
import product from './modules/product'
import shoppingCart from './modules/shoppingCart'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    price,
    product,
    shoppingCart
  }
})
