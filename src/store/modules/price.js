import axios from 'axios'

const state = {
    quotation: {}
}

const getters = {
    allItemsInQuotation: (state) => state.quotation
}

const actions = {
    async getQuotation({commit}, quantity){
        var payload = {}
        
        quantity.forEach(item => {
            payload[item.product.id.toString()] = item.quantity
        });
        
        const response = await axios.post('http://localhost:8081/api/price/calculate',payload)
        commit('setQuotation', response.data)
    }
}

const mutations = {
    setQuotation: (state, quotation) => (state.quotation = quotation)
}

export default {
    state,
    getters,
    actions,
    mutations
}