import axios from 'axios'

const state = {
    products: [],
    unitPrices:[]
}

const getters = {
    allProducts: (state) => state.products,
    allUnitPrices: (state) => state.unitPrices
}

const actions = {
    async fetchUnitPrices({commit}, id){
        const respose = await axios.get(`http://localhost:8081/api/price/list?productId=${id}&startPos=1&endPos=50`)
        commit('setUnitPrices', respose.data)
    },
    async fetchProductDetails({commit}){
        const respose = await axios.get('http://localhost:8081/api/products')
        commit('setProductDetails', respose.data)
    }
}

const mutations = {
    setUnitPrices: (state, prices) => (state.unitPrices = prices),
    setProductDetails: (state, products) => (state.products = products)
}

export default {
    state,
    getters,
    actions,
    mutations
}