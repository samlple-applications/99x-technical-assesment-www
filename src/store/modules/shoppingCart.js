const state = {
    cart: []
}

const getters = {
    allItemsInCart: (state) => state.cart
}

const actions = {
    addItemToCart({commit},item){
        commit('setItemToCart',item)
    },
    deleteItemFromCart({commit},id){
        commit('deleteItem',id)
    }
}

const mutations = {
    setItemToCart: (state,item) => (state.cart.push(item)),
    deleteItem:(state,id) => state.cart = state.cart.filter(item => item.product.id !== id)

}

export default {
    state,
    getters,
    actions,
    mutations
}